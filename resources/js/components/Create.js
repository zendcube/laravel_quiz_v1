import React, { Component } from 'react';

const Step_1 = (props) => (
    <form>
        <div className="form-group">
            <label htmlFor="numberOfGuests">Number of guests</label>
            <input
                id="numberOfGuests"
                className="form-control"
                name="numberOfGuests"
                type="number"
                value={props.numberOfGuests}
                onChange={props.action} />
        </div>
        <div className="form-group form-check">
            <div className="custom-control custom-radio custom-control-inline">
                <input
                    className="custom-control-input"
                    checked={props.gender === "male"}
                    onChange={props.action}
                    value="male"
                    type="radio"
                    id="genderMale"
                    name="gender"  />
                <label className="custom-control-label" htmlFor="genderMale">Male</label>
            </div>
            <div className="custom-control custom-radio custom-control-inline">
                <input
                    className="custom-control-input"
                    checked={props.gender === "female"}
                    onChange={props.action}
                    value="female"
                    type="radio"
                    id="genderFemale"
                    name="gender" />
                <label className="custom-control-label" htmlFor="genderFemale">Female</label>
            </div>
        </div>
    </form>
);

export default class Create extends Component {
    constructor(props) {
        super(props);
        this.state = {
          steps: 0,
          isValidated: false,
          gender: "female",
          numberOfGuests: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleNext = this.handleNext.bind(this);        
        this.handleBack = this.handleBack.bind(this);        
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        },this.checkValidation);
    }

    handleNext(event) {
        event.preventDefault();

        this.setState(prevState => ({
            steps: prevState.steps + 1
        }), this.checkValidation);
    }

    handleBack(event) {
        event.preventDefault();

        this.setState(prevState => ({
            steps: prevState.steps -1
        }), this.checkValidation);
    }

    checkValidation() {
        console.log(this.state.steps)
        switch(this.state.steps) {
            case 0:
            console.log('checkValidation', this.state.steps)
                if(this.state.gender && this.state.numberOfGuests.length) {
                    return  this.setState({
                        isValidated: true
                    });
                }
                return this.setState({
                    isValidated: false
                });
            default:
            console.log('checkValidation', this.state.steps)
                return this.setState({
                    isValidated: false
                });
        }
    }

    componentDidMount(){
        document.title = "Create :: Friendship Dare";
    }

    renderSwitch(param) {
        switch(param) {
            case 0:
                return <Step_1 action={this.handleInputChange} gender={this.state.gender} numberOfGuests={this.state.numberOfGuests}/>;
            case 1:
            // console.log(param)
                return 'bar 1';
            default:
            // console.log(param)
                return 'Something went wrong';
        }
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Create Your Own Friendship Dare</div>
                            <div className="card-body">{this.renderSwitch(this.state.steps)}</div>
                        </div>
                    </div>
                </div>
                <div className="row justify-content-center mt-3">
                    <div className="col-md-8">
                        <div className="row justify-content-center">
                        { this.state.steps > 0 &&
                            <div className="col-auto">
                                <button type="button" className="btn btn-secondary btn-lg" onClick={this.handleBack}>Back</button>
                            </div> 
                        }
                        { this.state.isValidated &&
                            <div className="col-auto">
                                <button type="button" className="btn btn-lg btn-primary" onClick={this.handleNext}>Next</button>
                            </div>
                        }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}