import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

export default class Header extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        $('.navbar-nav .nav-link').on('click', function () {
            $('#navbarNav').collapse('hide');
        });
    }

    render() {
        return (
            <nav className="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                <Link className="navbar-brand" to="/">Friendship Dare</Link>

                <div className="ml-auto" id="navbarNav">
                    <ul className="navbar-nav mr-auto">
                        {/* <li className="nav-item">
                            <NavLink exact activeClassName="active" className="nav-link" to="/">RESULT</NavLink>
                        </li> */}
                        <li className="nav-item">
                            <NavLink activeClassName="active" className="nav-link" to="/create">CREATE</NavLink>
                        </li>
                    </ul>
                </div>


                {/* <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink exact activeClassName="active" className='nav-link' to='/'>Home</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink activeClassName="active" className='nav-link' to='/about'>About</NavLink>
                    </li>
                    </ul>
                </div> */}
            </nav>
        );
    }
}
