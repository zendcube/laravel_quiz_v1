import React, { Component } from 'react';

export default class About extends Component {
    componentDidMount(){
        document.title = "About"
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">About Component</div>

                            <div className="card-body">I'm an about component!</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}