import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'
import Home from './components/Home';
import About from './components/About';
import Create from './components/Create';
import NoMatch from './components/NoMatch';

export default class Routes extends Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/about" component={About} />
                <Route path="/create" component={Create} />
                <Route component={NoMatch} />
            </Switch>
        );
    }
}