import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Routes from '../Routes';
import Header from '../components/Header';

export default class Main extends Component {
    render() {
        return (
            <BrowserRouter>
                <header>
                    <Header />
                </header>
                <main role="main">
                    <Routes />
                </main>
            </BrowserRouter>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<Main />, document.getElementById('app'));
}
